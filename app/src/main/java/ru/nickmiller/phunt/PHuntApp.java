package ru.nickmiller.phunt;

import android.app.Application;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.graphics.Typeface;
import android.os.PersistableBundle;
import android.util.Log;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.nickmiller.phunt.api.PHuntApi;
import ru.nickmiller.phunt.services.NotifierService;


public class PHuntApp extends Application {
    private static final String TAG = "phunt_app";
    private static final int NOTIFIER_JOB_ID = 5847;  // magic number
    private static final String ROOT_URL = "https://api.producthunt.com";
    public static final String ACCESS_TOKEN = "591f99547f569b05ba7d8777e2e0824eea16c440292cce1f8dfb3952cc9937ff";
    private static final String TF_ROBOTO_URI = "fonts/Roboto-Medium.ttf";

    public static Typeface tfRoboto;

    private JobInfo.Builder builder;
    private JobScheduler scheduler;
    private static PHuntApi api;
    private Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api = retrofit.create(PHuntApi.class);

        tfRoboto = Typeface.createFromAsset(getAssets(), TF_ROBOTO_URI);


        /*
         * Initialize JobScheduler service with NotifierService.class for notifying every half an hour.
         */
        scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        builder = new JobInfo.Builder(NOTIFIER_JOB_ID, new ComponentName(this, NotifierService.class))
                .setPersisted(true)
                .setPeriodic(30*60*1000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setRequiresDeviceIdle(false)
                .setRequiresCharging(false);
        scheduleJob();
    }

    public void scheduleJob() {
        int status = scheduler.schedule(builder.build());
        Log.d(TAG, "Job has scheduled with status " + status);
    }

    public static PHuntApi getApi() {
        return api;
    }
}
