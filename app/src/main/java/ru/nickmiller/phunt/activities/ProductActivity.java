package ru.nickmiller.phunt.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;
import ru.nickmiller.phunt.R;
import ru.nickmiller.phunt.views.RTextView;

public class ProductActivity extends AppCompatActivity {
    public static final String POST_URL = "post_url";
    public static final String PRODUCT_NAME = "product_name";
    public static final String PRODUCT_DESC = "product_description";
    public static final String PRODUCT_UPVOTES = "product_upvotes";
    public static final String PRODUCT_SCREENSHOT = "product_screenshot";

    private static String pUrl;
    private AVLoadingIndicatorView progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        progress = (AVLoadingIndicatorView) findViewById(R.id.sh_loading_progress);
        String pName = getIntent().getStringExtra(PRODUCT_NAME);
        String pDesc = getIntent().getStringExtra(PRODUCT_DESC);
        String pUpvotes = getIntent().getStringExtra(PRODUCT_UPVOTES);
        pUrl = getIntent().getStringExtra(POST_URL);
        String pScreen = getIntent().getStringExtra(PRODUCT_SCREENSHOT);
        Log.d("product_activity", pScreen);
        progress.smoothToShow();
        ImageView screenshot = (ImageView) findViewById(R.id.product_screenshot);
        Picasso.with(getApplicationContext()).load(pScreen)
                .into(screenshot, new Callback() {
                    @Override
                    public void onSuccess() {
                        progress.smoothToHide();
                    }

                    @Override
                    public void onError() {
                        progress.smoothToHide();
                    }
                });
        RTextView name = (RTextView) findViewById(R.id.product_name);
        name.setText(pName);
        RTextView description = (RTextView) findViewById(R.id.product_description);
        description.setText(pDesc);
        RTextView upvotes = (RTextView) findViewById(R.id.product_upvotes);
        upvotes.setText(pUpvotes);
    }

    public void onGetProductClick(View view) {
        Intent intent = new Intent(ProductActivity.this, WebActivity.class);
        intent.putExtra(POST_URL, pUrl);
        startActivity(intent);
    }
}
