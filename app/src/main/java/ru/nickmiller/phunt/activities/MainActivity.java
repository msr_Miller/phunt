package ru.nickmiller.phunt.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import org.w3c.dom.Text;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nickmiller.phunt.DateParser;
import ru.nickmiller.phunt.PHuntApp;
import ru.nickmiller.phunt.R;
import ru.nickmiller.phunt.adapters.PostsAdapter;
import ru.nickmiller.phunt.api.PHuntApi;
import ru.nickmiller.phunt.models.Post;

import java.text.ParseException;
import java.util.*;

import static ru.nickmiller.phunt.PHuntApp.ACCESS_TOKEN;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "main_activity";
    private static String CURRENT_TOPIC = "tech";
    private RecyclerView postsRecycler;
    private PostsAdapter adapter;
    private SwipeRefreshLayout refreshLayout;


    private PHuntApi api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeViews();
        api = PHuntApp.getApi();
        updatePosts(CURRENT_TOPIC);
    }


    /**
     * Look for "today" posts with chosen topic (default "tech")
     *
     * @param topic
     */
    private void updatePosts(String topic) {
        refreshLayout.setRefreshing(true);
        api.getPosts(topic, ACCESS_TOKEN).enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                List<Post.Product> products = new ArrayList<>();
                Date latestPostTime = DateParser.getLatestPostTime(getApplicationContext());
                Log.d(TAG, "Saved time: " + latestPostTime.toString());
                try {
                    products = response.body().getProducts();
                    for (Post.Product product : products) {
                        Date curTime = DateParser.parse(product.getTime());
                        latestPostTime = latestPostTime.after(curTime) ? latestPostTime : curTime;
                    }
                    DateParser.setLatestPostTime(getApplicationContext(), latestPostTime);
                    Log.d(TAG, latestPostTime.toString());
                } catch (NullPointerException | ParseException e) {
                    Log.wtf(TAG, e);
                }
                TextView infofMsg = (TextView) findViewById(R.id.info_msg);
                if (products.isEmpty()) {
                    infofMsg.setText(getString(R.string.empty_products_msg));
                    infofMsg.setVisibility(View.VISIBLE);
                } else {
                    infofMsg.setVisibility(View.GONE);
                }
                adapter = new PostsAdapter(products, (product) -> {
                    Intent intent = new Intent(MainActivity.this, ProductActivity.class);
                    intent.putExtra(ProductActivity.PRODUCT_NAME, product.getProductName());
                    intent.putExtra(ProductActivity.PRODUCT_DESC, product.getProductDescription());
                    intent.putExtra(ProductActivity.PRODUCT_UPVOTES, String.valueOf(product.getUpvotesCount()));
                    intent.putExtra(ProductActivity.POST_URL, product.getProductUrl());
                    intent.putExtra(ProductActivity.PRODUCT_SCREENSHOT, product.getScreenshot().getScreenshotUrl());
                    startActivity(intent);
                });
                postsRecycler.setAdapter(adapter);
                refreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                refreshLayout.setRefreshing(false);
                Log.wtf(TAG, t);
            }
        });
    }

    private void initializeViews() {
        postsRecycler = (RecyclerView) findViewById(R.id.posts_container);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        postsRecycler.setLayoutManager(manager);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        refreshLayout.setOnRefreshListener(() -> updatePosts(CURRENT_TOPIC));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Spinner spinner = (Spinner) toolbar.findViewById(R.id.topics_list);
        ArrayAdapter<CharSequence> sAdapter = ArrayAdapter.createFromResource(this, R.array.topics, R.layout.spinner_item);
        spinner.setAdapter(sAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String itemName = adapterView.getItemAtPosition(i).toString();
                updatePosts(itemName);
                CURRENT_TOPIC = itemName;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

}
