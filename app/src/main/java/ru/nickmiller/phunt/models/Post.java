package ru.nickmiller.phunt.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Post {

    @SerializedName("posts")
    @Expose
    List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> posts) {
        this.products = posts;
    }

    public class Product {

        @SerializedName("created_at")
        @Expose
        private String time;

        @SerializedName("id")
        @Expose
        private int id;

        @SerializedName("name")
        @Expose
        private String productName;

        @SerializedName("tagline")
        @Expose
        private String productDescription;

        @SerializedName("votes_count")
        @Expose
        private int upvotesCount;

        @SerializedName("thumbnail")
        @Expose
        private Thumbnail thumbnail;

        @SerializedName("redirect_url")
        @Expose
        private String productUrl;

        @SerializedName("screenshot_url")
        @Expose
        private Screenshot screenshot;


        public int getId() {
            return id;
        }

        public String getTime() {
            return time;
        }

        public Screenshot getScreenshot() {
            return screenshot;
        }

        public String getProductName() {
            return productName;
        }

        public String getProductDescription() {
            return productDescription;
        }

        public int getUpvotesCount() {
            return upvotesCount;
        }

        public Thumbnail getThumbnail() {
            return thumbnail;
        }

        public String getProductUrl() {
            return productUrl;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }

        public void setUpvotesCount(int upvotesCount) {
            this.upvotesCount = upvotesCount;
        }

        public void setThumbnail(Thumbnail thumbnailUri) {
            this.thumbnail = thumbnailUri;
        }

        public void setScreenshot(Screenshot screenshot) {
            this.screenshot = screenshot;
        }

        public void setProductUrl(String productUrl) {
            this.productUrl = productUrl;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public void setId(int id) {
            this.id = id;
        }

        public class Thumbnail {

            @SerializedName("image_url")
            @Expose
            private String imageUrl;


            public String getImageUrl() {
                return imageUrl;
            }

            public void setImageUrl(String imageUrl) {
                this.imageUrl = imageUrl;
            }

        }

        public class Screenshot {

            @SerializedName("300px")
            @Expose
            private String screenshotUrl;

            public String getScreenshotUrl() {
                return screenshotUrl;
            }

            public void setScreenshotUrl(String screenshotUrl) {
                this.screenshotUrl = screenshotUrl;
            }
        }

    }
}
