package ru.nickmiller.phunt.api;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.nickmiller.phunt.models.Post;

public interface PHuntApi {

    @GET("/v1/categories/{category_name}/posts")
    Call<Post> getPosts(@Path("category_name") String categoryName, @Query("access_token") String token);

}
