package ru.nickmiller.phunt.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import ru.nickmiller.phunt.R;
import ru.nickmiller.phunt.models.Post;
import ru.nickmiller.phunt.views.RTextView;

import java.util.List;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {

    private List<Post.Product> products;
    private PostsAdapter.OnPostClickListener listener;
    private Context context;

    public PostsAdapter(List<Post.Product> products, PostsAdapter.OnPostClickListener listener) {
        this.products = products;
        this.listener = listener;
    }

    @Override
    public PostsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.post_card, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Post.Product product = products.get(position);
        holder.pName.setText(product.getProductName());
        holder.pDescription.setText(product.getProductDescription());
        holder.pVotes.setText(String.valueOf(product.getUpvotesCount()));
        Picasso.with(context).load(product.getThumbnail().getImageUrl()).into(holder.pThumbnail);
        holder.itemView.setOnClickListener(view -> listener.onThemeClick(product));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RTextView pName, pDescription, pVotes;
        ImageView pThumbnail;

        public ViewHolder(View itemView) {
            super(itemView);
            context =  itemView.getContext();
            pName = (RTextView) itemView.findViewById(R.id.post_name);
            pDescription = (RTextView) itemView.findViewById(R.id.post_description);
            pVotes = (RTextView) itemView.findViewById(R.id.post_upvotes);
            pThumbnail = (ImageView) itemView.findViewById(R.id.post_thumbnail);
        }
    }

    public interface OnPostClickListener {
        void onThemeClick(Post.Product post);
    }

}
