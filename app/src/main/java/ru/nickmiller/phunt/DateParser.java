package ru.nickmiller.phunt;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;


public class DateParser {
    private static SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS",Locale.US);


    /**
     * Convert date which are got from the ProductHunt api.
     *
     * @param source
     * @return Date in an appropriate format
     * @throws ParseException
     */
    public static Date parse(String source) throws ParseException {
        return dateParser.parse(source);
    }


    /**
     * Save the time of the lust seeing posted product
     *
     * @param context
     * @param latestTime
     */
    public static void setLatestPostTime(Context context, Date latestTime) {
        String formatedDate = dateParser.format(latestTime);
        SharedPreferences pref = context.getSharedPreferences(context.getString(R.string.NOTIF_PREFERENCES), MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(context.getString(R.string.LATEST_PRODUCT_TIME), formatedDate);
        editor.apply();
    }


    /**
     * Get the time of the lust seeing posted product
     *
     * @param context
     * @return
     */
    public static Date getLatestPostTime(Context context) {
        SharedPreferences pref = context.getSharedPreferences(context.getString(R.string.NOTIF_PREFERENCES), MODE_PRIVATE);
        Date time;
        try {
            time = dateParser.parse(pref.getString(context.getString(R.string.LATEST_PRODUCT_TIME), dateParser.format(new Date(0))));
        } catch (ParseException e) {
            Log.w("date_parser", e);
            time = new Date(0);
        }
        return time;
    }
}
