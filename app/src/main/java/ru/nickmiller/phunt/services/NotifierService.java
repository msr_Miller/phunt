package ru.nickmiller.phunt.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import retrofit2.Response;
import ru.nickmiller.phunt.DateParser;
import ru.nickmiller.phunt.PHuntApp;
import ru.nickmiller.phunt.R;
import ru.nickmiller.phunt.activities.MainActivity;
import ru.nickmiller.phunt.activities.ProductActivity;
import ru.nickmiller.phunt.models.Post;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class NotifierService extends JobService {
    private static final String TAG = "job_service";
    private String[] topics;

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        topics = getApplicationContext().getResources().getStringArray(R.array.topics);
        new UpdatePosts().execute(jobParameters);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return true;
    }

    private class UpdatePosts extends AsyncTask<JobParameters, Void, Void> {

        @Override
        protected Void doInBackground(JobParameters... jobParameters) {
            try {
                List<Post.Product> freshProducts = checkForUpdates();
                if (!freshProducts.isEmpty()) {
                    notifyWithUpdates(freshProducts);
                }
            } catch (IOException e) {
                Log.wtf(TAG, e);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            jobFinished(jobParameters[0], false);
            return null;
        }
    }


    /**
     * Send push notification with fresh products.
     * If there is only one new product show all information about it. On click it open ProductActivity.
     * If there are more than one new product show number of them in content message. On click it open MainActivity.
     *
     * @param freshProducts
     */
    private void notifyWithUpdates(List<Post.Product> freshProducts) {
        Intent intent;
        String title, content;
        if (freshProducts.size() == 1) {
            Post.Product product = freshProducts.get(0);
            title = product.getProductName().equals("") ? "No description" : product.getProductName();
            content = product.getProductDescription();
            intent = new Intent(NotifierService.this, ProductActivity.class);
            intent.putExtra(ProductActivity.PRODUCT_NAME, product.getProductName());
            intent.putExtra(ProductActivity.PRODUCT_DESC, product.getProductDescription());
            intent.putExtra(ProductActivity.PRODUCT_UPVOTES, String.valueOf(product.getUpvotesCount()));
            intent.putExtra(ProductActivity.POST_URL, product.getProductUrl());
            intent.putExtra(ProductActivity.PRODUCT_SCREENSHOT, product.getScreenshot().getScreenshotUrl());
        } else {
            title = getString(R.string.app_name);
            intent = new Intent(NotifierService.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            content = String.format(Locale.US, getString(R.string.fresh_products_num), freshProducts.size());
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(NotifierService.this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri soundUri = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Notification.Builder notiBuilder = new Notification.Builder(NotifierService.this)
                .setContentTitle(title)
                .setStyle(new Notification.BigTextStyle().bigText(content))
                .setVibrate(new long[] {250, 250, 250 ,250})
                .setLights(Color.YELLOW, 1000, 1000)
                .setSmallIcon(R.drawable.ic_notif_icon)
                .setColor(getResources().getColor(R.color.mainColor))
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, notiBuilder.build());
    }


    /**
     * Check for new products using time of posting it.
     * If there are any new products save new time of latest product into shared preferences.
     *
     * @return List of products which was added after last "seeing" them
     * @throws IOException
     * @throws ParseException
     */
    private List<Post.Product> checkForUpdates() throws IOException, ParseException {
        List<Post.Product> freshProducts = new ArrayList<>();
        Date currentLatestTime = DateParser.getLatestPostTime(getApplicationContext());
        Date postTime = null;
        Date latestTime = currentLatestTime;
        for (String topic : topics) {
            Response<Post> response = PHuntApp.getApi().getPosts(topic, PHuntApp.ACCESS_TOKEN).execute();
            List<Post.Product> currentProducts = response.body().getProducts();
            for (Post.Product product : currentProducts) {
                postTime = DateParser.parse(product.getTime());
                if (postTime.after(currentLatestTime)) {
                    if (postTime.after(latestTime)) latestTime = postTime;
                    freshProducts.add(product);
                    Log.d(TAG, "New product: " + postTime.toString());
                }
            }
        }
        if (!freshProducts.isEmpty()) {
            DateParser.setLatestPostTime(getApplicationContext(), latestTime);
            Log.d(TAG, "New date has saved: " + postTime.toString());
        }
        return freshProducts;
    }

}
