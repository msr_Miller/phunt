package ru.nickmiller.phunt.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import static ru.nickmiller.phunt.PHuntApp.tfRoboto;

/**
 *  Custom TextView which uses Roboto font in common
 */

public class RTextView extends TextView {

    public RTextView(Context context) {
        super(context);
        initView();
    }

    public RTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public RTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        if (!isInEditMode()) {
            setTypeface(tfRoboto);
        }
    }
}
